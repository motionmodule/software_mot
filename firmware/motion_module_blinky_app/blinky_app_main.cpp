#include "stm32l4xx_hal.h"
#include "cmsis_os.h"

extern UART_HandleTypeDef huart2;

extern "C" void startDefaultTask(void const * argument) {
  uint16_t length;
  char message[42];
  uint32_t counter;

  while (true) {
    HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
    length = sprintf(message, "Alive %lu\r\n", ++counter);
    HAL_UART_Transmit(&huart2, (uint8_t*)message, length, 50);
    vTaskDelay(1000);
  }

}
